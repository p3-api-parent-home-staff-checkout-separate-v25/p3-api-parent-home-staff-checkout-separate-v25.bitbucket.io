/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9233421978210518, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.14321608040201006, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.9998566065283137, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [0.8508342922899885, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.6605679956308028, 500, 1500, "getNotifications"], "isController": false}, {"data": [0.0, 500, 1500, "getHomefeed"], "isController": false}, {"data": [0.7358695652173913, 500, 1500, "me"], "isController": false}, {"data": [0.9177833078101072, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [0.7386848847139197, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [0.8539988492520139, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.0, 500, 1500, "getChildCheckInCheckOut"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 58285, 0, 0.0, 257.55636956335275, 9, 20692, 32.0, 532.0, 1412.9500000000007, 2539.9600000000064, 191.64502022161577, 520.078090321162, 213.95209606603262], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getCountCheckInOutChildren", 796, 0, 0.0, 1891.2939698492453, 588, 3816, 1828.5, 2567.3, 2736.3, 3033.289999999999, 2.631596347502959, 1.662575718316704, 3.25608649637329], "isController": false}, {"data": ["getLatestMobileVersion", 38356, 0, 0.0, 38.700568359578575, 9, 1379, 25.0, 64.0, 113.0, 201.0, 127.87038271769569, 85.53829312658355, 94.27943257017603], "isController": false}, {"data": ["findAllConfigByCategory", 3476, 0, 0.0, 430.9401611047177, 30, 2788, 212.0, 1160.0, 1523.2999999999993, 2064.69, 11.517523136106243, 13.024699015245146, 14.959282979512992], "isController": false}, {"data": ["getNotifications", 1831, 0, 0.0, 820.604041507374, 81, 3595, 308.0, 2195.8, 2410.5999999999985, 2826.0, 6.066409123137172, 51.461271094521344, 6.74769530395824], "isController": false}, {"data": ["getHomefeed", 85, 0, 0.0, 17828.800000000003, 7015, 20692, 18538.0, 19877.2, 20062.5, 20692.0, 0.27948574622694244, 4.887998270681945, 1.398520472330911], "isController": false}, {"data": ["me", 2300, 0, 0.0, 654.1752173913048, 76, 3480, 268.5, 1912.5000000000005, 2110.749999999999, 2512.8799999999974, 7.6186690516413265, 10.03548736708735, 24.1431455786876], "isController": false}, {"data": ["getAllClassInfo", 5224, 0, 0.0, 286.25248851454774, 17, 2817, 167.0, 724.0, 1015.0, 1696.25, 17.360557239325782, 10.036867480152006, 44.57119627166747], "isController": false}, {"data": ["findAllChildrenByParent", 2342, 0, 0.0, 642.2241673783099, 53, 3521, 248.0, 1967.0, 2173.3999999999996, 2537.260000000003, 7.757484216732582, 8.712018407463349, 12.393793143139165], "isController": false}, {"data": ["findAllSchoolConfig", 3476, 0, 0.0, 430.0618527042577, 36, 2762, 222.5, 1119.3000000000002, 1478.3499999999967, 2062.23, 11.58388653389853, 252.41624627276954, 8.495604284138473], "isController": false}, {"data": ["getChildCheckInCheckOut", 399, 0, 0.0, 3776.7218045112786, 2231, 6366, 3866.0, 4848.0, 5050.0, 6024.0, 1.3185070138624986, 88.02450682488475, 6.067192430976653], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 58285, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
